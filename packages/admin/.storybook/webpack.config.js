const CircularDependencyPlugin = require('circular-dependency-plugin');

module.exports = ({config, mode}) => {
    config.resolve.extensions.push('.ts', '.tsx');
    
    config.module.rules.push({
        test: /\.tsx?$/,
        use: [
            {
                loader: 'babel-loader',
                options: {
                    cacheDirectory: true,
                    envName: 'development'
                }
            },
            {
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                    happyPackMode: true
                }
            }
        ]
    });

    config.module.rules.push({
        test: /\.stories\.tsx?$/,
        loaders: [
            {
                loader: require.resolve('@storybook/addon-storysource/loader'),
                options: { parser: 'typescript' }
            }            
        ],
        enforce: 'pre',
    });
    
    config.plugins.push(new CircularDependencyPlugin({
        exclude: /node_modules/,
        failOnError: true
    }));
    
    return config;
};