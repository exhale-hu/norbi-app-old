module.exports = {
  "env": {
    "production": {
      "plugins": [
        ["babel-plugin-styled-components", {
          "pure": true,
          "transpileTemplateLiterals": false
        }],
       ["reflective-bind/babel"/*, {"log": "debug"}*/],
        "@babel/plugin-syntax-dynamic-import",
        "@babel/plugin-syntax-object-rest-spread",
        "@babel/plugin-transform-react-jsx-self",
        "@babel/plugin-transform-react-jsx-source",
        "@babel/syntax-jsx",
        ["@babel/plugin-transform-react-jsx", {
          "useBuiltins": true
        }],
        ["@babel/plugin-transform-runtime", {
          "useESModules": true
        }]
      ]
    },
    "development": {
      "plugins": [
        ["babel-plugin-styled-components", {
          "displayName": true,
          "minify": false,
          "transpileTemplateLiterals": false
        }],
       ["reflective-bind/babel"/*, {"log": "debug"}*/],
        "@babel/plugin-syntax-dynamic-import",
        "@babel/plugin-syntax-object-rest-spread",
        "@babel/plugin-transform-react-jsx-self",
        "@babel/plugin-transform-react-jsx-source",
        // "react-hot-loader/babel",
        // "@babel/syntax-jsx",
        ["@babel/plugin-transform-react-jsx", {
          "useBuiltins": true
        }],
        ["@babel/plugin-transform-runtime", {
            "useESModules": true
        }]
      ]
    }
  }
};