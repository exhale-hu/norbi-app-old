import React, {createContext, ReactNode, useEffect, useLayoutEffect, useState} from 'react';
import {identity} from 'lodash-es';

export const AccordionContext = createContext<AccordionContextType>({
    exists: false,
    active: null,
    setActive: identity,
    animate: false
});

export interface AccordionContextType
{
    exists: boolean;
    active: number|string|null;
    setActive(value: number|string|null, animate: boolean): void;
    animate: boolean;
}

export interface AccordionProps
{
    children?: ReactNode;
}

export function Accordion({children}: AccordionProps)
{
    const [contextValue, setContextValue] = useState<AccordionContextType>(() => ({
        exists: true,
        active: null,
        setActive(value, animate) {
            setContextValue({
                ...contextValue,
                active: value,
                animate
            });
        },
        animate: false
    }));
    
    return <AccordionContext.Provider value={contextValue}>
        {children}
    </AccordionContext.Provider>;
}