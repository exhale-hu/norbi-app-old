import React, {ReactNode} from 'react';
import styled from 'styled-components';

const CardHeader = styled.div`
    color: white;
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: ${props => props.theme.sidebarBackgroundColor};
    border-bottom: 1px solid #23282c;
`;

const CardBody = styled.div`
    flex: 1 1 auto;
    padding: 1.25rem;
    color: white;
    background-color: ${props => props.theme.sidebarCollapsibleBackgroundColor};
`;

const CardWrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    background-color: ${props => props.theme.sidebarBackgroundColor};
    background-clip: border-box;
    margin-top: 1rem;
    border: 1px solid #23282c;
    border-radius: .25rem;
`;

export function Card({title, body}: { title: ReactNode, body: ReactNode })
{
    return <CardWrapper>
        <CardHeader>
            {title}
        </CardHeader>
        <CardBody>
            {body}
        </CardBody>
    </CardWrapper>;
}