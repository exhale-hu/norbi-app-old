import React from 'react';
import {styled} from '../../themes/theme';
import LoadingSpinner from '../../assets/loading-spinner.svg';

const LoadingWrapper = styled.div<{isLoading: boolean}>`
    display: ${props => props.isLoading ? 'flex' : 'hidden'};
    justify-content: center;
    align-items: center;
`;

const Loader = styled.img<{ isLoading: boolean }>`
    width: ${props =>props.isLoading ? 'auto' : '0'}
`;

export function LoadingIndicator({isLoading}: {isLoading: boolean})
{
    return <LoadingWrapper isLoading={isLoading}>
        <Loader isLoading={isLoading} src={LoadingSpinner} />
    </LoadingWrapper>;
}