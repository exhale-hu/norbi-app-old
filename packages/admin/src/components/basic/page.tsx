import {styled} from '../../themes/theme';

export const Page = styled.div`
  padding: 1.2rem;
`;

export const PageTitle = styled.h1`
  border-bottom: 1px solid ${props => props.theme.pageTitleBorderColor};
  padding-bottom: .7rem;
  margin-bottom: .7rem;
`;

export const PageContent = styled.div``;