import React, {ReactNode} from 'react';
import {styled} from '../../themes/theme';
import {identity} from 'lodash-es';
import {CaretLeft, CaretRight, StepBackward, StepForward} from 'styled-icons/fa-solid';

export type PageChangeHandler = (value: PaginationState) => void;

export interface PaginationState
{
    currentPage: number;
    totalItems: number;
    itemsPerPage: number;
}

export interface PaginatorProps extends PaginationState
{
    value: PaginationState;
    beforeAfterPages?: number;
    onChange?: PageChangeHandler;
}

export function Paginator({value, beforeAfterPages = 2, onChange = identity}: PaginatorProps)
{
    const { currentPage, itemsPerPage, totalItems } = value;
    
    const pageCount = Math.ceil(totalItems / itemsPerPage);
    
    return <StyledPaginator>
        <PageButton isDisabled={currentPage <= 0} onClick={() => onChange({ ...value, currentPage: 0 })}>
            <StepBackward />
        </PageButton>
        <PageButton isDisabled={currentPage <= 0} onClick={() => onChange({ ...value, currentPage: currentPage - 1 })}>
            <CaretLeft />
        </PageButton>
        
        {renderNumberedPageButtons(value, onChange, pageCount, beforeAfterPages)}

        <PageButton isDisabled={pageCount - 1 <= currentPage} onClick={() => onChange({ ...value, currentPage: currentPage + 1 })}>
            <CaretRight />
        </PageButton>
        <PageButton isDisabled={pageCount - 1 <= currentPage} onClick={() => onChange({ ...value, currentPage: pageCount - 1 })}>
            <StepForward />
        </PageButton>
    </StyledPaginator>;
}

function renderNumberedPageButtons(value: PaginationState, onChange: PageChangeHandler, pageCount: number,
                                   beforeAfterPages: number)
{
    const { currentPage } = value;
    
    const startIndex = Math.max(0, currentPage - beforeAfterPages);
    const endIndex = Math.min(pageCount, currentPage + beforeAfterPages);
    
    const nodes: ReactNode[] = [];
    for (let index = startIndex; index <= endIndex; index++)
    {
        // Local variable for callback to capture
        const currentPage = index;
        nodes.push(<PageButton key={index} onClick={() => onChange({ ...value, currentPage })}>
            {index + 1}
        </PageButton>);
    }
    
    return nodes;
}

const StyledPaginator = styled.div`
    display: flex;
`;

const PageButton = styled.div<{isDisabled?: boolean, isHidden?: boolean, isSelected?: boolean}>`
    ${props => props.isHidden ? 'display: none;' : ''};
    border: solid .1rem lightgrey;
    padding: 0 .3rem;
    margin: .5rem .2rem;
    cursor: ${props => props.isDisabled || props.isSelected ? 'default' : 'pointer'};
    font-weight: ${props => props.isSelected ? 'bold' : '0'};
`;