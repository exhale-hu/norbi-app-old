import React, {ReactNode, useCallback, useContext, useRef} from 'react';
import {NavLink, NavLinkProps} from 'react-router-dom';
import {styled} from '../../themes/theme';
import {Header} from './header';
import {List} from './list';
import {CollapsibleContext} from './collapsible';

export interface SidebarProps
{
    headerText?: string;
    children?: ReactNode;
}

export function Sidebar({headerText, children}: SidebarProps)
{
    return <SidebarElement>
        <SidebarHeader>
            {headerText}
        </SidebarHeader>

        <List>
            {children}
        </List>
    </SidebarElement>;
}

export function SidebarLink(props: NavLinkProps)
{
    const isInitialCall = useRef(true);
    
    const collapsibleContext = useContext(CollapsibleContext);
    const isActiveCallback = useCallback((match, location) => {
        if (match)
            collapsibleContext.setActive(true, !isInitialCall.current);        
        
        isInitialCall.current = false;
        return !!match;
    }, [collapsibleContext]);
    
    return <li>
        <StyledNavLink isActive={isActiveCallback} {...props} />
    </li>;
}

export const StyledNavLink = styled(NavLink)`
  display: block;
  padding: .8rem 1.5rem;
  transition: background-color .2s ease-in-out;
  color: ${props => props.theme.sidebarLinkColor};
  outline: none;
  text-decoration: none !important;

  &:hover {
    background-color: ${props => props.theme.sidebarHoverBackgroundColor};
    color: ${props => props.theme.activeSidebarLinkColor};
  }

  &.active {
    color: ${props => props.theme.activeSidebarLinkColor};
  }
`;

export const SidebarElement = styled.div`
    height: 100vh;
    background-color: ${props => props.theme.sidebarBackgroundColor};
    width: 24rem;
    min-width: 24rem;
    
    font-size: 1.3rem;
`;

export const SidebarHeader = styled(Header)`
  justify-content: center;
  font-size: 1.8rem;
  
  background-color: ${props => props.theme.sidebarHeaderBackgroundColor};
`;

