import React, {Key} from 'react';
import {styled} from '../../themes/theme';
import {StyledIcon} from 'styled-icons/types';
import {ArrowSortedDown, ArrowSortedUp, ArrowUnsorted} from 'styled-icons/typicons';
import {StyledIconBase} from 'styled-icons/StyledIconBase';
import {Column, SortState} from './data-grid';

export type SortDirection = 'asc' | 'desc';

export type OnSortCallback<T> = (newSort: SortState<T>) => void;

export interface HeadCellProps<T>
{
    column: Column<T>;
    sort: SortDirection|null;
    onSort: OnSortCallback<T>;
}

export function HeadCell<T>({column, sort, onSort}: HeadCellProps<T>)
{
    let SortIcon: StyledIcon;
    
    switch (sort)
    {
        case 'asc':
            SortIcon = ArrowSortedUp;
            break;
        case 'desc':
            SortIcon = ArrowSortedDown;
            break;
        default:
            SortIcon = ArrowUnsorted;
            break;
    }
    
    function onSortClick(event: React.MouseEvent<HTMLDivElement>)
    {
        if (!column.sortable)
            return;
        
        let newDirection: SortDirection;
        
        switch (sort)
        {
            case 'asc':
                newDirection = 'desc';
                break;
            case 'desc':
            default:
                newDirection = 'asc';
                break;
        }
        
        onSort({ column, direction: newDirection });
    }
    
    function onMouseDown(event: React.MouseEvent<HTMLDivElement>)
    {
        // Prevent selection on double click
        // Source: https://stackoverflow.com/a/43321596
        if (1 < event.nativeEvent.detail)
        {
            event.preventDefault();
        }
    }
    
    return <StyledHeadCell onClick={onSortClick} onMouseDown={onMouseDown}>
        <span>{column.displayName}</span>
        {column.sortable && <SortIcon />}
    </StyledHeadCell>;
}

export function getHeadCellKey<T>(column: Column<T>, index: number): Key
{
    return column.propertyName || column.displayName || index;
}

const StyledHeadCell = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 .5rem;
    border-bottom: solid .2rem lightGrey;
    font-weight: 700;
    font-size: 1.5rem;
    cursor: pointer;
    
    & + & {
      border-left: solid .2rem lightgray;
    }
    
    ${StyledIconBase} {
      font-size: 1.2em;
    }
`;