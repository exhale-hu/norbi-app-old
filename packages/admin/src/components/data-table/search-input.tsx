import {styled} from '../../themes/theme';
import React from 'react';

export interface SearchInputProps
{
    value: string;
    onChange?(value: string): void;
}

export function SearchInput({value, onChange}: SearchInputProps)
{
    function onChangeCallback(event: React.ChangeEvent<HTMLInputElement> | string) {
        const newValue = typeof event === 'string' ? event : event.currentTarget.value;

        onChange && onChange(newValue);
    }
    
    return <StyledDataTableSearch>
        <GridSearchInput
            type="text"
            value={value}
            onChange={onChangeCallback}
        />
        <GridClearButton onClick={() => onChangeCallback('')}>Clear</GridClearButton>
    </StyledDataTableSearch>;
}

export const StyledDataTableSearch = styled.div`
    display: flex;
    align-items: center;
    margin: 1rem;
`;

export const GridSearchInput = styled.input`
    margin-right: 1rem;
    height: 4rem;
    width: 25rem;
    font-size: 2rem;
    padding: 0 1rem;
    border: solid black 1px;
    border-radius: 1rem;
    
    :focus {
        outline: none;
    }
`;
export const GridClearButton = styled.div`
    margin-right: 1rem;
    border: solid black 1px;
    padding: 1rem;
    border-radius: 1rem;
    cursor: pointer;
`;