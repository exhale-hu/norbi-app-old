import React, {ReactNode, useCallback} from 'react';
import {Formik, FormikActions} from 'formik';
import styled from 'styled-components';
import {LoadingIndicator} from '../basic/loading-indicator';

interface EditTemplateProps
{
    formTitle: string;
    values: any[];
    onSave: (values: any[]) => any;
    children?: ReactNode;

    isDataLoading?: boolean;
    validationSchema?: any;
    saveButtonText?: string;
    saveButtonComponent?: any;
}

export function EditTemplatePage(props: EditTemplateProps)
{
    const onSubmit = useCallback((values: any, actions: FormikActions<any>) => {
        try
        {
            props.onSave(values);
        }
        catch (e)
        {
            console.error('Error during saving data!');
        }
    }, []);
    
    return <PageWrapper>
        <PageBox>
            {props.formTitle}
        </PageBox>
        {!props.isDataLoading && <PageBody>
            <Formik
                initialValues={props.values}
                onSubmit={onSubmit}
                validationSchema={props.validationSchema}
                render={formikProps => (
                    <form onSubmit={formikProps.handleSubmit}>
                        {props.children}

                        <PageBox>
                            {props.saveButtonComponent ? props.saveButtonComponent :
                                <button type='submit'>
                                    {props.saveButtonText ? props.saveButtonText : 'Mentés'}
                                </button>
                            }
                        </PageBox>
                    </form>
                )}
            />
        </PageBody>}
        {props.isDataLoading && <LoadingIndicator isLoading={true} />}
    </PageWrapper>;
}

const PageWrapper = styled.div`

`;

const PageBox = styled.div`
    padding: .75rem 1.25rem;
    background-color: #f0f3f5;
    border-bottom: 1px solid #c8ced3;
`;

const PageBody = styled.div`
    padding: 1.25rem;
`;
