import React from 'react';
import styled from 'styled-components';
import { bind } from '@epicentric/utilbelt-react';

interface NumberInputProps 
{
    onChange?: (change: React.ChangeEvent<HTMLInputElement>) => void;
    defaultValue: number;
    minValue: number;
    maxValue: number;
}

export class NumberInput extends React.Component<NumberInputProps>
{
    public constructor(props: NumberInputProps)
    {
        super(props);
    }

    @bind
    public handleKeyPress(e: React.KeyboardEvent<HTMLInputElement>)
    {
        // TODO: test this for further stuff
        
        if (e.key === 'e' || e.key === '+' || e.key === '-')
            e.preventDefault();
    }
    
    public render()
    {
        return (
        <div>
            <InputField type='number'
                        onChange={this.props.onChange}
                        onKeyPress={this.handleKeyPress}
                        defaultValue={this.props.defaultValue.toString()}
                        min={this.props.minValue}
                        max={this.props.maxValue}
            />
        </div>
        );
    }

}

const InputField = styled.input`
    display: flex;
    color: black;
    background-color: #e4e4e4;
    border: 2px solid transparent;
    border-radius: 5px;
    outline: none;
    width: 60px;
    height: 30px;
    text-align: center;
    font-size: 14px;
    :hover{
        border-color: lightblue;
        transition: .3s linear; 
    }
`;