import {Page, PageContent, PageTitle} from '../basic/page';
import React from 'react';
import {Place} from '../../models/place';
import {Column, DataGrid} from '../data-table/data-grid';
import {useCollection, useCollectionData} from 'react-firebase-hooks/firestore';
import {firestore} from 'firebase';

const columns: Column<Place>[] = [
    { displayName: 'Név', propertyName: 'name', sortable: true },
    { displayName: 'Utoljára módosítva', propertyName: 'lastUpdated', sortable: true },
    { displayName: 'Cím', propertyName: 'address', sortable: true },
    { displayName: 'Műveletek', render(value: string, item) { return 'Hello there'; } }
];

const places: Place[] = [];

for (let i = 1; i <= 20; i++)
{
    places.push({ id: i, name: `Place ${i}`, address: `Main street ${i}` });
}

export function PlacesListPage()
{
    const [places] = useCollectionData<Place>(firestore().collection('places'));
    
    return <Page>
        <PageTitle>Helyek</PageTitle>
        <PageContent>
            <DataGrid
                columns={columns}
                items={places}
                itemKey={place => place.id}
            />
        </PageContent>
    </Page>;
}

// export class PlacesList extends BaseComponent
// {
//     @tracked
//     private headers: Column[] = [];
//     @tracked
//     private places: Place[] = [];
//     @tracked
//     private displayedPlaces: Place[] = [];
//     @tracked
//     private isLoading: boolean = false;
//     @tracked
//     private paginator: PaginationOptions = {
//         currentIndex: 0,
//         length: 6,
//         isServerSide: false,
//         itemsPerPage: 3
//     };
//    
//     public constructor(props: {}, context: any)
//     {
//         super(props, context);
//    
//         // const query = firestore.collection('places').orderBy('name');
//         //
//         // this.addObservable('places', observableFromQuery(query).pipe(
//         //     map(snapshot => snapshot.docs.map(doc => doc.data))
//         // ));
//    
//         this.places = [
//             {
//                 id: 0,
//                 name: 'Teszt létesítmény 1',
//                 location: {
//                     latitude: 47.6063187,
//                     longitude: 19.0592206,
//                 },
//                 description: 'Teszt létesítmény leírás 1',
//             },
//             {
//                 id: 1,
//                 name: 'Teszt létesítmény 2',
//                 location: {
//                     latitude: 50.6063187,
//                     longitude: 17.0592206,
//                 },
//                 description: 'Teszt létesítmény leírás 2',
//             },
//             {
//                 id: 2,
//                 name: 'Teszt létesítmény 3',
//                 location: {
//                     latitude: 39.6063187,
//                     longitude: 21.0592206,
//                 },
//                 description: 'Teszt létesítmény leírás 3',
//             },
//             {
//                 id: 3,
//                 name: 'Teszt létesítmény 4',
//                 location: {
//                     latitude: 39.6063187,
//                     longitude: 21.0592206,
//                 },
//                 description: 'Teszt létesítmény leírás 4',
//             },
//             {
//                 id: 4,
//                 name: 'Teszt létesítmény 5',
//                 location: {
//                     latitude: 39.6063187,
//                     longitude: 21.0592206,
//                 },
//                 description: 'Teszt létesítmény leírás 5',
//             },
//             {
//                 id: 5,
//                 name: 'Teszt létesítmény 6',
//                 location: {
//                     latitude: 39.6063187,
//                     longitude: 21.0592206,
//                 },
//                 description: 'Teszt létesítmény leírás 6',
//             },
//         ];
//
//         this.isLoading = false;
//
//         this.displayedPlaces = this.places.slice(0, this.paginator.itemsPerPage);
//    
//         this.headers = [
//             {
//                 displayName: 'Név',
//                 propertyName: 'name',
//             },
//             {
//                 displayName: 'Hely',
//                 propertyName: 'location',
//                 customToString: (item: any): any =>
//                 {
//                     // return `lat: ${item.latitude} lng: ${item.longitude}`;
//                     return <div><p>lat: {item.latitude}</p><p>long: {item.longitude}</p></div>;
//                 }
//             },
//             {
//                 displayName: 'Leírás',
//                 propertyName: 'description',
//             },
//             {
//                 displayName: 'Műveletek',
//                 propertyName: 'actions',
//                 sortable: false,
//             },
//         ];
//     }
//
//     public render(): React.ReactNode
//     {
//         return <Page>
//             <PageTitle>Létesítmények</PageTitle>
//             <PageContent>
//                 <DataTable
//                     columns={this.headers}
//                     data={this.displayedPlaces}
//                     pagination={this.paginator}
//                     isLoading={this.isLoading}
//                     onPageChange={this.onPageChange}
//                     onSearchChange={this.onSearchChange}
//                     onSort={this.onSort.bind(this)}>
//                 </DataTable>
//             </PageContent>
//         </Page>;
//     }
//
//     @bind
//     private onSearchChange(result: SearchResult): boolean
//     {
//         console.debug('placesList onSearchChange start', {result});
//
//         const start = this.paginator.itemsPerPage * this.paginator.currentIndex;
//         console.log('placesList onSearchChange start page',
//             {paginator: this.paginator, page: this.paginator.currentIndex, start});
//         const displayedPlaces = this.places.slice(start, start + this.paginator.itemsPerPage);
//
//         if(!result.value)
//         {
//             console.log('placesList onSearchChange filter cleared', result.value);
//             this.displayedPlaces = displayedPlaces;
//             return true;
//         }
//
//         console.log('placesList onSearchChange filter names', result.value);
//         this.displayedPlaces = displayedPlaces.filter( item => {
//            return item.name.indexOf(result.value) !== -1;
//         });
//
//         return true;
//     }
//
//     @bind
//     private onPageChange(paginator: PaginationOptions, nextPage: number): boolean
//     {
//         console.debug('placesList onPageChange start', {paginator, nextPage, places: this.places});
//         this.isLoading = true;
//         const start = paginator.itemsPerPage * nextPage;
//         console.log('placesList onPageChange start', {paginator, nextPage, start});
//         this.displayedPlaces = this.places.slice(start, start + paginator.itemsPerPage);
//         console.log('placesList displayedPlaces', {displayedPlaces: this.displayedPlaces});
//         this.paginator.currentIndex = nextPage;
//         this.paginator = Object.assign({}, this.paginator);
//
//         // Simulate server response time
//         setTimeout(() => {
//             this.isLoading = false;
//         }, 3000);
//
//         return true;
//     }
//
//     @bind
//     private onSort(propertyName: string, direction: 'asc' | 'desc'): boolean
//     {
//         console.debug('placesList onSort start', {propertyName, direction, places: this.places});
//
//         const dir = direction === 'asc' ? -1 : 1;
//
//         this.places = this.places.sort(
//             (a: any, b: any) => {
//                 const aValue = a[propertyName];
//                 const bValue = b[propertyName];
//                 const cond = aValue > bValue;
//                 const result = (aValue > bValue) ? dir : -dir;
//
//                 console.debug('sort', {
//                     a, aValue, b, bValue,
//                     cond, dir, result,
//                 });
//                 return result;
//             }).slice();
//
//         console.log('placesList onSort', {propertyName, direction, places: this.places});
//         return true;
//     }
// }
