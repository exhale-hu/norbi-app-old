import 'reflect-metadata';
import {unmountComponentAtNode} from 'react-dom';
import React from 'react';
import * as ReactDOM from 'react-dom';
import {App} from './components/app';

let appElement = document.getElementById('app-container');

if (!appElement)
{
    appElement = document.createElement('div');
    appElement.id = 'app-container';
    appElement.classList.add('app');
    document.body.appendChild(appElement);
}
else
{
    unmountComponentAtNode(appElement);
}

ReactDOM.render(<App />, appElement);

// ReactDOM.render(
//     <React.StrictMode>
//         <App />
//     </React.StrictMode>,
// appElement
// );

// if (module.hot)
// {
//     module.hot.accept();
// }