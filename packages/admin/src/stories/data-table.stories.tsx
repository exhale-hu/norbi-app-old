import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs';
import React, {useMemo, useState} from 'react';
import {Place} from '../models/place';
import {GlobalStyles} from '../global-styles';
import {orderBy} from 'lodash-es';
import {Column, SortState} from '../components/data-table/data-grid';

function DataTableExample()
{
    const [sort, setSort] = useState<SortState<Place>|null>(null);
    const [searchValue, setSearchValue] = useState('');
    
    const columns: Column<Place>[] = [
        { displayName: 'Név', propertyName: 'name', sortable: true },
        { displayName: 'Utoljára módosítva', propertyName: 'lastUpdated', sortable: true },
        { displayName: 'Cím', propertyName: 'address', sortable: true }, 
        { displayName: 'Műveletek', render(value: string, item) { return 'Hello there'; } }
    ];
    
    const places = useMemo(() => {
        let generatedPlaces: Place[] = [];
        
        for (let i = 1; i <= 20; i++)
        {
            generatedPlaces.push({ id: i, name: `Place ${i}`, address: `Main street ${i}` });
        }
        
        if (searchValue)
        {
            const search = searchValue.trim();
            
            generatedPlaces = generatedPlaces.filter(place => place.address.toLocaleLowerCase().includes(search)
                || place.name.toLocaleLowerCase().includes(search));
        }
        
        if (sort)
            return orderBy(generatedPlaces, [sort.column.propertyName], [sort.direction]) as Place[];
        
        return generatedPlaces;
    }, [sort, searchValue]);

    return <>
        <GlobalStyles />
        {/*<DataTable*/}
        {/*    columns={columns}*/}
        {/*    items={places}*/}
        {/*    itemKey={place => place.id}*/}
        {/*    sort={sort}*/}
        {/*    onSort={setSort}*/}
        {/*    searchEnabled*/}
        {/*    searchValue={searchValue}*/}
        {/*    onSearchValueChange={setSearchValue}*/}
        {/*/>*/}
    </>;
}

storiesOf('DataTable', module)
    .addDecorator(withKnobs)
    .add('DataTable example', () => <DataTableExample />);