import * as baseStyled from 'styled-components';
import {ThemedStyledComponentsModule} from 'styled-components';

export interface Theme
{
    // General
    headerBackgroundColor: string;
    
    // Sidebar
    sidebarHeaderBackgroundColor: string;
    sidebarHoverBackgroundColor: string;
    sidebarCollapsibleBackgroundColor: string;
    sidebarLinkColor: string;
    activeSidebarLinkColor: string;
    sidebarBackgroundColor: string;
    
    // Page
    pageTitleBorderColor: string;
}

// Based on: https://gist.github.com/chrislopresto/490ef5692621177ac71c424543702bbb
const { default: styled, createGlobalStyle, css } = baseStyled as ThemedStyledComponentsModule<Theme>;

export { styled, createGlobalStyle, css };