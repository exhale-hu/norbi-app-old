declare module '@ckeditor/ckeditor5-build-classic'
{
    import {Class} from 'utility-types';
    import {Editor} from '@ckeditor/ckeditor5-react';
    
    const defaultExport: Class<Editor>;
    
    export default defaultExport;
}