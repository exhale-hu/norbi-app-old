declare module '@ckeditor/ckeditor5-react'
{
    import {Class} from 'utility-types';
    
    export interface Editor
    {
        getData(): string;
    }
    
    // TODO finish typing for Editor and Config
    export interface CKEditorProps
    {
        editor: Class<Editor>;
        data: string;
        config?: object;
        onInit?: (editor: Editor) => void;
        onChange?: (eventInfo: EventInfo, editor: Editor) => void;
        onBlur?: (editor: Editor) => void;
        onFocus?: (editor: Editor) => void;
        disabled?: boolean;
    }

    export interface EventInfo
    {
        name: string;
        path: object[];
        return: unknown;
        source: object;
    }

    declare class CKEditor extends React.Component<CKEditorProps> 
    {
        public constructor(props = {}, context = {})
        {
            super(props, context);
        }
    }

    export default CKEditor;
}