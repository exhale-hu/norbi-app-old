import {Ref, useEffect, useLayoutEffect, useRef, useState} from 'react';
import ResizeObserver from 'resize-observer-polyfill';

export interface MeasureBounds
{
    left: number;
    top: number;
    width: number;
    height: number;
}

// Based on: https://codesandbox.io/embed/q3ypxr5yp4
export function useMeasure<TElement extends HTMLElement = HTMLElement>(): [ { ref: Ref<TElement> }, MeasureBounds ]
{
    const ref = useRef<TElement>(null);
    
    const [bounds, setBounds] = useState({ left: 0, top: 0, width: 0, height: 0 });
    const [resizeObserver] = useState(() => new ResizeObserver(([entry]) => setBounds(entry.contentRect)));
    
    useLayoutEffect(() => {
        const current = ref.current;
        
        if (!current)
            return;
        
        resizeObserver.observe(current);
        
        return () => resizeObserver.unobserve(current);
    }, [ref.current]);
    
    // Cleanup observer on unmount
    useEffect(() => () => resizeObserver.disconnect(), []);
    
    return [{ ref }, bounds];
}
