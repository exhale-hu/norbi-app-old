import {BaseComponent} from '../base-component';
import {Omit, Route, RouteProps} from 'react-router';
import * as React from 'react';
import {tracked} from '../../decorators/tracked';
import {LoadingIndicator} from '../loading-indicator/loading-indicator';

interface LazyRouteProps extends Omit<RouteProps, 'component'>
{
    lazyComponent: (props: RouteProps) => Promise<React.ComponentType<any>>;
}

export class LazyRoute extends BaseComponent<LazyRouteProps>
{    
    @tracked
    private component: React.ComponentType<any>|null;
    private started: boolean = false;

    public componentDidUpdate(prevProps: Readonly<LazyRouteProps>, prevState: Readonly<Dictionary<any>>, prevContext: any): void
    {
        super.componentDidUpdate(prevProps, prevState, prevContext);
        
        if (prevProps.lazyComponent !== this.props.lazyComponent)
        {
            this.component = null;
            this.started = false;
        }
    }

    public render(): React.ReactNode
    {
        return <Route {...this.props} render={props => {
            if (!this.component && !this.started)
            {
                this.started = true;
                this.props.lazyComponent(props).then(result => this.component = result);
            }
            
            if (!this.component)
            {
                // These are inlined to prevent blinking
                return <div style={{
                    position: 'fixed',
                    left: 0,
                    top: 0,
                    width: '100%',
                    height: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <LoadingIndicator style={{width: '30px', height: '30px'}} />
                </div>;
            }

            const Component = this.component;
            return <Component {...props} />;
        }} />;
    }
}