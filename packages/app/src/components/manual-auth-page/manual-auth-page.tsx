import React from 'react';
import { BaseComponent } from '../base-component';
import './_manual-auth-page.scss';
import { withRouter, Link, Redirect } from 'react-router-dom';
import { tracked } from '../../decorators/tracked';
import { extract, parse } from 'query-string';
import {Formik, Form, Field, FieldProps, FormikErrors} from 'formik';
import { translate } from '../../utils/translate';
import { bind } from '../../decorators/bind';
import { SignupFormFields } from '../../models/user';
import { firebaseServ } from '../../services/firebase-service';


export class ManualAuthPage extends BaseComponent 
{
    @tracked
    private params: any;

    @tracked
    private isLogIn: boolean;

    public componentWillMount()
    {
        translate.setLanguage("hu");
        this.setAuthMode();
    }

    public render(): React.ReactNode 
    {
        return ( 
        <div className='manual-auth-wrapper'>
            <div className="manual-auth-content">
                <div className="header">
                    <div className="back-button">
                        <Link to='/auth-page' > <button>Back</button> </Link>
                    </div>
                    <div className="location-text">
                        {this.isLogIn ? translate.translate("logIn") : translate.translate("signUp") }
                    </div>
                </div>
                <Formik initialValues={{ fullName: '', email: '', password: '', password2: '', birthDate: '', acceptedTOS: false }}
                    validate={this.validateForm}
                    onSubmit={(values: SignupFormFields) => this.onSubmit(values)}
                >
                {({errors, touched}) => (
                    <Form className="form-wrapper">
                        <div className="form-fields-wrapper">
                            <div className="input-field-wrapper">
                                <Field hidden={this.isLogIn} type="text" name="fullName" placeholder="Full Name" className='input-field' />
                            </div>
                            <div className="input-field-wrapper">
                                <Field type="email" name="email" placeholder="Email" className='input-field' />
                            </div>
                            <div className="input-field-wrapper">
                                <Field type="password" name="password" placeholder="Password" className='input-field' />
                            </div>
                            <div className="input-field-wrapper">
                                <Field hidden={this.isLogIn} type="password" name="password2" placeholder="Password again" className='input-field' />
                                {touched.password2 && errors.password2 && <div>{errors.password2}</div>}
                            </div>
                            <div className="input-field-wrapper">
                                <Field hidden={this.isLogIn} type="date" name="birthDate" placeholder="Birth date" className='input-field' />
                            </div>
                        </div>
                        <div className="bottom-disclaimer-wrapper">
                            <div hidden={this.isLogIn} className="terms-box">
                                <Field type="checkbox" name="acceptedTOS" className='tos-checkbox' />
                                <span className='tos-text'>{translate.translate("acceptedTOS")}</span>
                            </div>
                            <div className="auth-button">
                                <button type='submit'>{this.isLogIn ? translate.translate("logIn") : translate.translate("signUp")}</button>
                            </div>
                            <div className="bottom-text-wrapper">
                                <p hidden={this.isLogIn}>
                                    {translate.translate('alreadyHaveAccount')}
                                    <Link to='/manual-auth' onClick={() => this.setIsLogin(true)}> <span>{translate.translate('logIn')}</span> </Link>
                                </p>
                                <div hidden={!this.isLogIn}>
                                    <Link to='/forgot-password'> {translate.translate("forgotPassword")} </Link>
                                    <br />
                                    <Link to='/manual-auth?login=true' onClick={() => this.setIsLogin(false)}> {translate.translate("signUp")} </Link>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </div>
        </div>
        );
    }

    private setAuthMode(): void
    {
        this.params = parse(extract(this.props.location.search));
        this.isLogIn = this.params.login && this.params.login === "true";
    }
    
    private setIsLogin(state: boolean)
    {
        this.isLogIn = state;
    }

    @bind
    private validateForm(values: SignupFormFields)
    {
        if(this.isLogIn)
            return {};
        
        let errors: FormikErrors<SignupFormFields> = {};

        // todo get actual error messages
        if ( !values.acceptedTOS)
            errors.acceptedTOS = 'You must accept the TOS!';
        if (values.password !== values.password2)
            errors.password2 = 'The passwords do not match!';

        console.debug('manual-auth-page validateForm errors', errors);
        return errors;
        // todo validate more
    }

    private async onSubmit(values: SignupFormFields)
    {
        console.debug('manual-auth-page onSubmit start', values);
        if (this.isLogIn)
        {
            await firebaseServ.loginWithEmail(values);
            // todo check result
        }
        else
        {
            await firebaseServ.registerWithEmail(values);
            // todo check result
        }

    }
}