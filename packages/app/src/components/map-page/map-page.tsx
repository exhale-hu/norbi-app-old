import React from 'react';
import './_map-page.scss';
import { BaseComponent } from '../base-component';
import { NavigationBar } from '../navigation-bar/navigation-bar';
import { ExtendedMarker, RawMarker } from '../../models/map-interfaces';
import { CameraPosition, GoogleMapOptions, ILatLng, MyLocation } from '@ionic-native/google-maps';
import { RouteComponentProps } from 'react-router';
import { MapService } from '../../services/map-service';

export class MapPage extends BaseComponent<RouteComponentProps<any>>
{
    private mapServ: MapService = new MapService();
    private mapWrapper: HTMLDivElement | null;
    private mapOptions: GoogleMapOptions = {
        controls: {
            compass: true,
            indoorPicker: false,
            myLocationButton: true,
            myLocation: true,     // (blue dot)
            zoom: false,          // android only
            mapToolbar: false     // android only
        },
        preferences: {
            
            padding: {
                left: 0,
                top: 0,
                bottom: 0,
                right: 0
            },
            
            building: true
        }
    };
    
    public constructor(props: RouteComponentProps<any>, context?: {})
    {
        super(props, context);
        
        this.state.selectedMarker = null;
    }
    
    public async componentDidMount(): Promise<void>
    {
        this.mapServ.createMap(this.mapWrapper as HTMLDivElement, this.mapOptions);
        
        this.mapServ.onMapClicked.subscribe((position: CameraPosition<ILatLng>) =>
        {
            this.setState({ selectedMarker: null });
            this.mapServ.setMapPadding(0, 0, 0, 0);
        });
        
        let location: MyLocation | null = await this.mapServ.getLocation();
        console.log('Current location', location);
        
        // Go to current location
        if (location)
        {
            this.mapServ.moveCamera({
                target: location.latLng,
                zoom: 17
            });
        }
        
        // TODO: remove adn get markers data from server
        // Add random markers
        for (let i = 0; i < 100; i++)
        {
            let rawMarker: RawMarker = {
                id: i,
                lat: 47.4945611 + ((Math.random() * 0.2) - 0.1),
                lng: 19.0495026 + ((Math.random() * 0.2) - 0.1),
                title: 'Nev ' + i,
                description: 'Leiras ' + i
            };
            
            let marker: ExtendedMarker | null = this.mapServ.addMarker({
                position: { lat: rawMarker.lat, lng: rawMarker.lng },
                disableAutoPan: true
            });
            
            if (!marker)
            {
                continue;
            }
            
            marker.payload = rawMarker;
            
            marker.onMarkerClicked.subscribe((extendedMarker: ExtendedMarker) =>
            {
                console.log('Marker[' + extendedMarker.source.getId() + '] clicked', marker);
                this.setState({ selectedMarker: extendedMarker });
                this.mapServ.setMapPadding(0, 0, 120, 0);
            });
        }
    }
    
    public render(): React.ReactNode
    {
        return <div className="map-page">
            <div className="map-wrapper" ref={ref => this.mapWrapper = ref}>
                {this.state.selectedMarker && <div className="marker-info">
                    <img className="picture" src=""/>
                    <div className="info-wrapper">
                        <div className="title">{this.state.selectedMarker.payload.title}</div>
                        <div className="description">{this.state.selectedMarker.payload.description}</div>
                        {/*TODO:*/}
                        <div className="other-info">Egyéb infó</div>
                    </div>
                </div>}
            </div>
            <NavigationBar/>
        </div>;
        
    }
}