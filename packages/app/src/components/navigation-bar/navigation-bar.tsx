import React from 'react';
import { BaseComponent } from '../base-component';
import { NavigationItem, NavigationItemProps } from '../navigation-item/navigation-item';
import './_navigation-bar.scss';

const navigationItemList: NavigationItemProps[] = [
    {
        name: 'Main',
        route: '/'
    },
    {
        name: 'Places',
        route: '/places'
    },
    {
        name: 'Search',
        route: '/search'
    },
    {
        name: 'Profile',
        route: '/profile'
    },
    {
        name: 'Map',
        route: '/map'
    }
]

export class NavigationBar extends BaseComponent
{   
    public render(): React.ReactNode
    {
        return (
        <div className='navigation-bar'>
            <div className='navigation-bar-content'>
            {
                navigationItemList.map( (value, index) => 
                    <NavigationItem name={value.name} route={value.route} key={index} />
                )
            }
            </div>
        </div>
        );
    }
}