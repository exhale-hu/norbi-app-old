import React from 'react';
import { NavLink } from 'react-router-dom';
import { BaseComponent } from '../base-component';
import './_navigation-item.scss';

export interface NavigationItemProps
{
    name: string,
    route: string,
    component?: BaseComponent,
}

export class NavigationItem extends BaseComponent<NavigationItemProps>
{
    public render(): React.ReactNode
    {
        return (
        <NavLink exact to={this.props.route} className='navigation-item' activeClassName='active'>
            <div className='navigation-item-content'>
                <span>{this.props.name}</span>
            </div>
        </NavLink>
        );
    }
}