import React from 'react';
import { BaseComponent } from '../base-component';
import { withRouter, Link } from 'react-router-dom';
import { bind } from '../../decorators/bind';
import { tracked } from '../../decorators/tracked';
import { translate, AvailableLanguage } from '../../utils/translate';
import styled from 'styled-components';
import { PageContent, PageWrapper } from '../../styles/global';

// Styles
const WelcomeInputWrapper = styled('div')`
    display: flex;
    flex-direction: column;
    align-items: center;
    `;

const LanguagePlaceholder = styled('span')<{ size: string}>`
    color: white;
    font-size: ${props => props.size};
    `;

const HelloText = styled(LanguagePlaceholder)<{ fadeType: 'fadein' | 'fadeout' }>`
    animation: ${props => props.fadeType} 2s;
    `;

const LanguageSelect = styled('select')`
    width: 18rem;
    font-size: 1.7rem;
    `;

const NextButtonWrapper = styled(Link)`
    display: flex;
    width: 100%;
    `;

const NextButton = styled('button')`
    align-self: center;
    margin: 0 auto;
    width: 10rem;
    height: 4rem;
    margin-top: 1.5rem;
    background: transparent;
    border: .2rem solid white;
    border-radius: .6rem;
    color: white;
    font-size: 1.4rem;
    transition-duration: 0.4s;
    cursor: pointer;
    // todo needs styling

    &:hover {
        background: white;
        color: #7c1c1d;
    }
    `;
export class WelcomePage extends BaseComponent
{
    // todo should be from translations
    public readonly helloTexts: string[] = [ 'Hello', 'Hi', 'こんにちは', '안녕하세요' ];
    
    @tracked
    public currentHelloText: string;
    
    @tracked
    public currentHelloTextIndex: number;
    
    @tracked
    public isFadingIn: boolean;
    
    
    public componentWillMount()
    {
        this.currentHelloTextIndex = 0;
        this.currentHelloText = this.helloTexts[ 0 ];
        this.isFadingIn = true;
    }
    
    public render(): React.ReactNode
    {
        return <PageWrapper>
            <PageContent>
                <HelloText
                    size={'5rem'}
                    fadeType={(this.isFadingIn ? 'fadein' : 'fadeout')}
                    onAnimationEnd={this.getNextHelloText}
                >
                    {this.currentHelloText}
                </HelloText>
                <img src='http://via.placeholder.com/300x300'/>
                <WelcomeInputWrapper>
                    <LanguagePlaceholder size={'2rem'}>
                        Choose your language...
                    </LanguagePlaceholder>
                    <LanguageSelect
                        onChange={this.onLanguageSelected}
                        placeholder='Select a language...'
                    >
                        <option value='en'>English</option>
                        <option value='en'>Chinese</option>
                        <option value='hu'>Japanese</option>
                    </LanguageSelect>
                </WelcomeInputWrapper>
                <NextButtonWrapper to='/auth-page'>
                    <NextButton type='button'>Next</NextButton>
                </NextButtonWrapper>
            </PageContent>
        </PageWrapper>;
    }
    
    @bind
    public getNextHelloText(): void
    {
        // The component should not update the text when it has faded in
        if (this.isFadingIn)
        {
            this.isFadingIn = !this.isFadingIn;
        }
        // only update it when its not visible
        else
        {
            let nextIndex = this.currentHelloTextIndex += 1;
            if (nextIndex >= this.helloTexts.length)
            {
                nextIndex = 0;
            }
            
            this.currentHelloTextIndex = nextIndex;
            this.currentHelloText = this.helloTexts[ nextIndex ];
            this.isFadingIn = !this.isFadingIn;
        }
    }
    
    @bind
    public onLanguageSelected(selected: any): void
    {
        translate.setLanguage(selected);
    }
}