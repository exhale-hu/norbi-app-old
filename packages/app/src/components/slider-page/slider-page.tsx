import React from 'react';
import { BaseComponent } from "../base-component";
import './_slider-page.scss';
import Slider from 'react-slick';
import {Settings} from "react-slick";

export class SliderPage extends BaseComponent
{

    public sliderSetting: Settings = {
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        focusOnSelect: false,
    };

    public render()
    {
        return (
            <div className="slider-page-wrapper">
                <div className="slider-page-content">
                    <div className="skip-button-wrapper">
                        <div className="skip-button">
                            <button>Skip</button>
                        </div>
                    </div>
                    <div className="slider-wrapper">
                        <Slider {...this.sliderSetting}>
                            <div className="slider-element">
                                <div className="content">
                                    <div className="text">
                                        <p>asd</p>
                                    </div>
                                    <div className="image">
                                        <img src='http://via.placeholder.com/400x150' />
                                    </div>
                                </div>
                            </div>
                            <div className="slider-element">
                                <div className="content">
                                    <div className="text">
                                        <p>asd</p>
                                    </div>
                                    <div className="image">
                                        <img src='http://via.placeholder.com/400x150' />
                                    </div>
                                </div>
                            </div>
                            <div className="slider-element">
                                <div className="content">
                                    <div className="text">
                                        <p>asd</p>
                                    </div>
                                    <div className="image">
                                        <img src='http://via.placeholder.com/400x150' />
                                    </div>
                                </div>
                            </div>
                            <div className="slider-element">
                                <div className="content">
                                    <div className="text">
                                        <p>asd</p>
                                    </div>
                                    <div className="image">
                                        <img src='http://via.placeholder.com/400x150' />
                                    </div>
                                </div>
                            </div>
                            <div className="slider-element">
                                <div className="content">
                                    <div className="text">
                                        <p>asd</p>
                                    </div>
                                    <div className="image">
                                        <img src='http://via.placeholder.com/400x150' />
                                    </div>
                                </div>
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
        );
    }
}