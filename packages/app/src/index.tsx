import './index.scss';
// import {connectToDevTools} from 'react-devtools-core';
// connectToDevTools({ host: '192.168.0.101' });
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Switch } from 'react-router-dom'
import {createMemoryHistory} from 'history';
import {Router} from 'react-router';
import { LazyRoute } from './components/lazy-route/lazy-route';

const appElement = document.createElement('div');
appElement.className = 'app';
const history = createMemoryHistory({
    initialEntries: ['/']
});

ReactDOM.render(
    <Router history={history}>
        <Switch>
            <LazyRoute
                exact
                path='/'
                lazyComponent={
                    () => import('./components/pages/welcome-page')
                        .then(result => result.WelcomePage)
                }
            />
            {/* TODO: Just for debug */}
            <LazyRoute
                exact
                path='/main'
                lazyComponent={
                    () => import('./components/main-page/main-page')
                        .then(result => result.MainPage)
                }
            />
            <LazyRoute
                path='/profile'
                lazyComponent={
                    () => import('./components/profile-page/profile-page')
                        .then(result => result.ProfilePage)
                }
            />
            <LazyRoute
                path='/auth-page'
                lazyComponent={
                    () => import('./components/auth-page/auth-page')
                        .then(result => result.AuthPage)
                }
            />
            <LazyRoute
                path='/manual-auth'
                lazyComponent={
                    () => import('./components/manual-auth-page/manual-auth-page')
                        .then(result => result.ManualAuthPage)
                }
            />
            <LazyRoute
                path='/slider-page'
                lazyComponent={
                    () => import('./components/slider-page/slider-page')
                        .then(result => result.SliderPage)
                }
            />
            <LazyRoute
                path='/map'
                lazyComponent={
                    () => import('./components/map-page/map-page')
                        .then(result => result.MapPage)
                }
            />
        </Switch>
    </Router>,
    document.body.appendChild(appElement)
);