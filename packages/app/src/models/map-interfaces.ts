import { GoogleMapsEvent, Marker } from '@ionic-native/google-maps';
import { Subject } from 'rxjs';

export interface RawMarker
{
    id: number;
    lat: number;
    lng: number;
    title?: string;
    description?: string;
}

export class ExtendedMarker
{
    public source: Marker;
    public payload: RawMarker;

    public readonly onMarkerClicked: Subject<ExtendedMarker> = new Subject();
    
    public constructor(marker: Marker)
    {
        this.source = marker;
        this.source.on(GoogleMapsEvent.MARKER_CLICK).subscribe( this.markerClickedHandler.bind(this));
    }
    
    private markerClickedHandler(data: any[]): void
    {
        console.log('Marker clicked', { marker: this, data: data });
        this.onMarkerClicked.next(this);
    }
}