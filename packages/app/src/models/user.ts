
export interface SignupFormFields
{
    fullName: string,
    email: string,
    password: string,
    password2: string,
    birthDate: string,
    acceptedTOS: boolean,
}

export class User
{
    full_name?: string;
    email?: string;
    birth_date?: string;
    created_at?: string;
    deleted_at?: string;
    
    constructor(data?: any)
    {
        this.full_name = data ? data.full_name || data.fullName : undefined;
        this.email = data ? data.email : undefined;
        this.birth_date = data ? data.birth_date || data.birthDate : undefined;
        this.created_at = data ? data.created_at : undefined;
        this.deleted_at = data ? data.deleted_at : undefined;
    }
    
    public toFireBaseObject(): object
    {
        return {
            full_name: this.full_name || null,
            email: this.email || null,
            birth_date: this.birth_date || null,
            created_at: this.created_at || null,
            deleted_at: this.deleted_at || null
        };
    }
}