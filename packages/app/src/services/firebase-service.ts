import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';
import { SignupFormFields, User } from '../models/user';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

const config = {
    apiKey: "AIzaSyAHQ2wHw-RVPbsjlpI0T-nMZo6KrAmJrPQ",
    authDomain: "norbi-app.firebaseapp.com",
    databaseURL: "https://norbi-app.firebaseio.com",
    projectId: "norbi-app",
    storageBucket: "norbi-app.appspot.com",
    messagingSenderId: "1071414553126",
    appId: "1:1071414553126:web:708013ef9aeeab6cf414e7"
};

interface GoogleResponse
{
    accessToken?: string;
    displayName?: string;
    email?: string;
    expires?: string;
    expires_in?: string;
    familyName?: string;
    givenName?: string;
    idToken?: string;
    imageUrl?: string;
    userId?: string;
}

export class FirebaseService
{
    private app: firebase.app.App;
    private database: firebase.firestore.Firestore;
    
    public constructor()
    {
        console.debug('Firebase Service initialize start', config);
        this.app = firebase.initializeApp(config);
        console.debug('Firebase Service initialize app', this.app);
        this.database = firebase.firestore();
        console.debug('Firebase Service initialize database', this.database);
        
    }
    
    public async registerWithEmail(data: SignupFormFields): Promise<any>
    {
        console.debug('Firebase Service registerWithEmail start', data);
        
        if(!data.email || !data.password)
        {
            console.debug('Firebase Service registerWithEmail missing data', data);
            return null;
        }
        
        try
        {
            const result: firebase.auth.UserCredential =
                await firebase.auth().createUserWithEmailAndPassword(data.email, data.password);
            console.debug('Firebase Service registerWithEmail success', result);
            
            const user: User = new User(data) ;
            user.created_at = result.user!.metadata.creationTime;
    
            await this.saveUserData(result.user!.uid, user);
            
            return result;
        }
        catch (error)
        {
            console.error('\'Firebase Service registerWithEmail failed', error);
        }
        
        return null
    }
    
    private async saveUserData(uid: string, user: User): Promise<User | null>
    {
        const data = user.toFireBaseObject();
        console.debug('Firebase Service saveUserData start', { uid, data });
        try
        {
            await this.database.collection('users').doc(uid)
                .set(data);
            console.debug('Firebase Service saveUserData success');
        }
        catch (error)
        {
            console.error('\'Firebase Service saveUserData failed', error);
            return null;
        }
        return user;
    }
    
    private async getUserData(uid: string): Promise<User | null>
    {
        console.debug('Firebase Service getUserData start', uid);
    
        try
        {
            const result: any = await this.database.collection('users').doc(uid).get();
            console.debug('Firebase Service getUserData success', result);
            return result;
        }
        catch (error)
        {
            console.error('\'Firebase Service getUserData failed', error);
            return null;
        }
    }
    
    public async loginWithEmail(user: SignupFormFields): Promise<any>
    {
        console.debug('Firebase Service loginWithEmail start', user);
        
        if(!user.email || !user.password)
        {
            console.debug('Firebase Service loginWithEmail missing data', user);
            return null;
        }
        
        try
        {
            const result = await firebase.auth().signInWithEmailAndPassword(user.email, user.password);
            console.debug('Firebase Service loginWithEmail success', result);
            return result;
        }
        catch (error)
        {
            console.error('\'Firebase Service loginWithEmail failed', error);
            return null
        }
    }
    
    public async loginWithFacebook(): Promise<User | null>
    {
        console.debug('Firebase Service loginWithFacebook start');
        
        let facebookResponse: FacebookLoginResponse | null = null;
        
        try
        {
            facebookResponse =  await Facebook.login(['email']);
            console.debug('Firebase Service loginWithFacebook facebookResponse', facebookResponse);
        }
       catch(error)
       {
           console.error('Firebase Service loginWithFacebook facebook get token failed', error);
           return null;
       }
        
        const facebookToken: string = facebookResponse.authResponse.accessToken;
        console.debug('Firebase Service loginWithFacebook facebookToken', facebookToken);
        
        const credential: firebase.auth.OAuthCredential =
            firebase.auth.FacebookAuthProvider.credential(facebookToken);
        console.debug('Firebase Service loginWithFacebook credential', credential);
        
        try
        {
            const response: firebase.auth.UserCredential =
                await firebase.auth().signInAndRetrieveDataWithCredential(credential);
            console.debug('Firebase Service loginWithFacebook response', response);
    
            if (response.additionalUserInfo!.isNewUser)
            {
                console.debug('Firebase Service loginWithFacebook new user');
        
                const user: User = new User(response.user);
                user.created_at = response.user!.metadata.creationTime;
        
                await this.saveUserData(response.user!.uid, user);
        
                return user;
            }
    
            try
            {
                const user: User | null = await this.getUserData(response.user!.uid);
                console.debug('Firebase Service loginWithFacebook get user data', user);
                
                return user;
            }
            catch(error)
            {
                console.error('Firebase Service loginWithFacebook get user data failed', error);
                return null;
            }
        }
        catch(error)
        {
            console.error('Firebase Service loginWithFacebook failed', error);
            return null;
        }
    }
    
    public async loginWithGoogle(): Promise<User | null>
    {
        console.debug('Firebase Service loginWithGoogle start');
        
        let googleResponse: GoogleResponse | null = null;
        
        try
        {
            const googleOption = {
                webClientId: config.webClientId,
            };
            
            googleResponse =  await GooglePlus.login(googleOption);
            console.debug('Firebase Service loginWithGoogle googleResponse', googleResponse);
        }
       catch(error)
       {
           console.error('Firebase Service loginWithGoogle google get token failed', error);
           return null;
       }
        
        const googleToken: string = googleResponse!.idToken!;
        console.debug('Firebase Service loginWithGoogle googleToken', googleToken);
        
        const credential: firebase.auth.OAuthCredential =
            firebase.auth.GoogleAuthProvider.credential(googleToken);
        console.debug('Firebase Service loginWithGoogle credential', credential);
        
        try
        {
            const response: firebase.auth.UserCredential =
                await firebase.auth().signInAndRetrieveDataWithCredential(credential);
            console.debug('Firebase Service loginWithGoogle response', response);
    
            if (response.additionalUserInfo!.isNewUser)
            {
                console.debug('Firebase Service loginWithGoogle new user');
        
                const user: User = new User(response.user);
                user.created_at = response.user!.metadata.creationTime;
                user.full_name = googleResponse!.displayName;
        
                await this.saveUserData(response.user!.uid, user);
        
                return user;
            }
    
            try
            {
                const user: User | null = await this.getUserData(response.user!.uid);
                console.debug('Firebase Service loginWithGoogle get user data', user);
                
                return user;
            }
            catch(error)
            {
                console.error('Firebase Service loginWithGoogle get user data failed', error);
                return null;
            }
        }
        catch(error)
        {
            console.error('Firebase Service loginWithGoogle failed', error);
        }
        return null;
    }
}

export const firebaseServ: FirebaseService = new FirebaseService();