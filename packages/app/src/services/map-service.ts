import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    MyLocation, MarkerOptions, CameraPosition, Marker, ILatLng
} from '@ionic-native/google-maps';
import { ExtendedMarker } from '../models/map-interfaces';
import { Subject } from 'rxjs';

export class MapService
{
    private map: GoogleMap | null = null;
    
    public mapWrapper: HTMLDivElement | null = null;
    public markers: ExtendedMarker[] = [];
    
    public readonly onMapClicked: Subject<CameraPosition<ILatLng>> = new Subject();
    public readonly onMapReady: Subject<GoogleMap[] | null> = new Subject();
    public readonly onMyLocationClicked: Subject<MyLocation> = new Subject();
    public readonly onMyLocationButtonClicked: Subject<void> = new Subject();
    
    public createMap(wrapper: HTMLDivElement, options: GoogleMapOptions): void
    {
        if (this.map)
        {
            this.map.destroy();
            console.warn('Map is destroyed, because it is already initialized');
        }
        
        try
        {
            this.mapWrapper = wrapper;
            this.markers = [];
            this.map = GoogleMaps.create(wrapper, options);
            
            this.map.on(GoogleMapsEvent.MAP_READY).subscribe(this.mapReadyHandler.bind(this));
            this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(this.mapClickedHandler.bind(this));
            
            this.map.addEventListener(GoogleMapsEvent.MY_LOCATION_CLICK).subscribe(
                this.myLocationClickedHandler.bind(this));
            
            this.map.addEventListener(GoogleMapsEvent.MY_LOCATION_BUTTON_CLICK).subscribe(
                this.myLocationButtonClickedHandler.bind(this));
        }
        catch (error)
        {
            console.error('Failed to create google map', { wrapper: wrapper, options: options, error: error });
        }
    }
    
    private mapReadyHandler(maps: GoogleMap[]): void
    {
        if(!maps || maps.length === 0)
        {
            console.error('Map ready data is missing', maps);
            this.onMapReady.next(null);
        }
    
        console.log('Map ready', maps);
        this.onMapReady.next(maps);
    }
    
    private mapClickedHandler(positions: CameraPosition<ILatLng>[]): void
    {
        console.log('Map clicked', positions);
        this.onMapClicked.next(positions[0]);
    }
    
    private myLocationClickedHandler(location: MyLocation): void
    {
        console.log('Map my location clicked', location);
        this.onMyLocationClicked.next(location);
    }
    
    private myLocationButtonClickedHandler(): void
    {
        console.log('Map my location button clicked');
        this.onMyLocationButtonClicked.next();
    }
    
    public async getLocation(): Promise<MyLocation | null>
    {
        if (!this.map)
        {
            console.error('Failed to get location, because map is not initialized',
                this.map);
            return null;
        }
        
        try
        {
            return await this.map.getMyLocation({ enableHighAccuracy: true });
        }
        catch (error)
        {
            console.error('Failed to get current location', error);
            return null;
        }
    }
    
    public addMarker(options: MarkerOptions): ExtendedMarker | null
    {
        if (!this.map)
        {
            console.error('Failed to add marker, because map is not initialized',
                { map: this.map, options: options });
            return null;
        }
        
        try
        {
            const marker: Marker = this.map.addMarkerSync(options);
            const extendedMarker = new ExtendedMarker(marker);
            
            this.markers.push(extendedMarker);
            return extendedMarker;
        }
        catch (error)
        {
            console.error('Failed to add marker', { option: options, error: error });
            return null;
        }
    }
    
    public moveCamera(position: CameraPosition<any>): void
    {
        if (!this.map)
        {
            console.error('Failed to move camera, because map is not initialized',
                { map: this.map, position: position });
            return;
        }
        
        this.map.moveCamera(position);
    }
    
    public setMapPadding(top: number, right?: number, bottom?: number, left?: number): void
    {
        if (!this.map)
        {
            console.error('Failed to set map padding, because map is not initialized', this.map);
            return;
        }
        
        this.map.setPadding(top, right, bottom, left);
    }
}

export const mapService = new MapService();