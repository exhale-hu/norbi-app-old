import styled from 'styled-components';

export const PageWrapper = styled('div')`
    width: 100%;
    height: 100%;
    min-height: 100%;

    background: linear-gradient(230deg, #e6e6e6, #125dbe, #e70303);
    background-size: 600% 600%;

    animation: background-animation 9s ease infinite;

    @keyframes background-animation {
        0%{background-position:0% 52%}
        50%{background-position:100% 49%}
        100%{background-position:0% 52%}
    }
    `;



export const PageContent = styled('div')`
    display: flex;
    height: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
    `;