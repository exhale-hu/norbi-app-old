declare module 'react-devtools-core' {
    export function connectToDevTools(host: {
        host?: string,
        port?: number,
        resolveRNStyle?: Function
    }): void;
}